module.exports = {
  devServer: {
    port: 8888, //项目运行端口号
    open: true, //编译自动打开浏览器
  },
  lintOnSave: false, //关闭eslint语法检查
}