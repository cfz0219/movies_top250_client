import Vue from 'vue'
import { 
  Button, 
  Table, 
  TableColumn, 
  Message, 
  MessageBox, 
  Tag,
  Pagination,
  Form,
  FormItem,
  Input,
  Checkbox,
  CheckboxGroup,
  Radio,
  RadioGroup,
  Upload,
} from 'element-ui'

Vue.use(Button);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Tag);
Vue.use(Pagination);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(Upload)

Vue.prototype.$message = Message;
Vue.prototype.$confirm = MessageBox;
