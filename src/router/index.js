import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect: '/home'
  },
  {
    path:'/home',
    component: () => import('@/views/HomePage')
  },
  {
    path:'/add',
    component: () => import('@/views/AddPage')
  },
  {
    path:'/detail',
    component: () => import('@/views/DetailPage')
  },
]

const router = new VueRouter({
  routes
})

export default router
